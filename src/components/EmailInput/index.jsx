import React from 'react'
import './index.css'
import { useState } from 'react'

const EmailInput = (props) => {

    const [ email, setEmail ] = useState('');

    function saveEmail() {
        const emails = email;
        console.log(emails)
        localStorage.setItem('emails', email)
    }

    return (
        <div className="email-input">
            <div className="nes-field is-inline">
                <input type="text" id="error_field" className="nes-input is-error" placeholder="Email" onChange={e => setEmail(e.target.value)} value={email}></input> 
            </div>
                <button type="button" className="nes-btn is-error" id="btn-email" onClick={saveEmail}>Enviar</button>
        </div>
    )
}

export default EmailInput
