import React from 'react'
import Card from '../Cards/index'
import EmailInput from '../EmailInput/index'

import './index.css'

import Background from '../../assets/images/bg.jpg'

export default function Content() {
    return (
        <>
            <div className="content" style={{ backgroundImage: `url(${Background})`}}>
                <div className="left-content">
                    <div className="title-left">
                        <h2>
                            Temos os melhores preços para você, amaente de <b style={{color: "red"}}>Video-Games!</b>
                        </h2>
                    </div>
                    <div className="container">
                        <Card xbox xboxPhoto />
                        <Card ps5 ps5Photo/>
                    </div>
                </div>

                <div className="right-content">
                        <div className="box-msg">
                            <p> E ainda está precisando de um Gift Card? Irá Rolar um sorteio e você só precisa cadastrar aí embaixo :) </p>
                            <EmailInput />
                        </div>
                </div>
            </div>
        </>
    )
}

