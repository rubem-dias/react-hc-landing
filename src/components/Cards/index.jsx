/* eslint-disable react/prop-types */ // TODO: upgrade to latest eslint tooling
import React from 'react'
import './index.css'

import Ps5PhotoStyle from '../../assets/images/ps5.png'
import XboxPhotoStyle from '../../assets/images/xone.png'

function getBackGroundDescription(props) {
    if(props.xbox) return "XboxOne"
    if(props.ps5) return "Ps5"
}

function getPhotoProduct(props) {
    if(props.xboxPhoto) {
        return (
                <img className="XboxImg" src={XboxPhotoStyle} />
        )
    }
    if(props.ps5Photo) {
        return (
                <img className="Ps5Img" src={Ps5PhotoStyle} />
        )
    }
}

function getProductName(props) {
    if(props.xboxPhoto) return "Xbox One"
    if(props.ps5) return "Playstation 5"
}

function getProductPrice(props) {
    if(props.xboxPhoto) return "R$1.999.90"
    if(props.ps5) return  "R$5.000.90"
}

const Card = (props) => {


    return (
        <div className="card">
            <span className={`Card ${getBackGroundDescription(props)}`} />
                <div className="imgBox">
                    {getPhotoProduct(props)}
                </div>
            <span className="productName"> {getProductName(props)} </span>
            <span className="price"> {getProductPrice(props)} </span>
            <button type="button" className="nes-btn is-error" id="btn-buy">Comprar Agora</button>        </div>
    )
}

export default Card