import React from 'react'
import './index.css'
import redMush from "./../../assets/images/redmush.png"

const Header = () => {


    return (
        <>
        
                <header className="header-bar">
                    <a href="/" className="header-brand">
                        <div className="aside-logo">
                            <img className="left-logo" src={redMush} alt="" />
                        </div>
                    </a>
                    <div className="middleTitle">
                        <h3> Gurutech </h3>
                    </div>
                </header>
            
        </>
    )
}

export default Header
