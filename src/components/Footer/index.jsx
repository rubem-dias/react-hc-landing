import React from 'react'
import './index.css'

const Footer = () => {
    return (
        <>
            <footer className="footer">
                <p id="middle-footer"> Created By: Rubem Dias <a href="https://github.com/rubem-dias" target="_blank" rel="noreferrer"><i className="nes-icon github is-small"></i></a></p>
            </footer>
        </>
    )
}

export default Footer
