import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import "nes.css/css/nes.min.css";
import '../node_modules/font-awesome/css/font-awesome.min.css'; 

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);